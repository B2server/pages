<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="/stylesheet.css">
	<title>Reticulum Network - Linux in a Bit's Stuff</title>
	<meta name="fediverse:creator" content="@Linux_in_a_Bit@infosec.exchange">
</head>
<body>
<header>
<a id="top"></a>
<h1><a class="title" href="/">
	<img src="/ChannelIcon.png" style="height: 1.2em; width: 1.2em; vertical-align: bottom; background: #141414;"> Linux in a Bit's Stuff
</a></h1>
<i>Posted 2024-08-17, Revised 2025-03-05</i>
</header>
<h2>The Reticulum Network and How it Works</h2>
<aside>This essay also has a video version on <a href="https://www.youtube.com/watch?v=q8ltLt5SK6A">YouTube</a> and <a href="https://spectra.video/w/unzScuHEYFFFkUHYSCkCZH">PeerTube</a></aside>
<details id="TLDR">
	<summary>TL;DR</summary>
	<p><i>Reticulum is a per-packet encrypted mesh network that flood routes only one kind of packet, called an announce. Announces tell each node in the network the “next hop” to get to every other node on the network. Everything else is routed along a single path determined by those “next hops,” resulting in very efficient routing overall.</i></p>
</details>
<details open id="Introduction">
	<summary>Introduction</summary>
	<p>Offline communication networks are often hard to establish, and previously, if you wanted to set up anything larger than a small scale community mesh network, you used to be basically out of luck; let alone if you wanted to do so on any kind of budget. Luckily there's a fantastic project that does exactly that. It's called Reticulum, and it's the most elegant solution for truly global scale peer-to-peer mesh networking I've seen.</p>
	<p>Reticulum has potential to become a legitimate replacement for the internet, and it is versatile enough to work over almost any networking medium including LoRa, serial, and packet radio. It's a full network stack down to the physical layer, everything is end to end encrypted per packet, and it's also surprisingly efficient.</p>
	<img src="2022-10-26_13-37-26.jpg"
	alt="A graphic comparing the OSI network model, the TCP/IP network model, and the Reticulum Network Stack network model. The two other models have many layers while the RNS model has 3; the physical layer, the secure extensable application layer, and application layer extensions."
	title="A graphic comparing the OSI network model, the TCP/IP network model, and the Reticulum Network Stack network model. The two other models have many layers while the RNS model has 3; the physical layer, the secure extensable application layer, and application layer extensions.">
	<p>With Reticulum, anyone can make and join self-organizing and almost completely unmanaged networks that can freely interconnect, and are very difficult to take down, surveil, or censor. Of course Reticulum <i>is</i> still in development, but it already works astonishingly well.</p>
	<p>Reticulum does not require you to store other people's data in some kind of distributed file system or data store; it is <i>exclusively</i> a network stack. There is <i>no</i> reliance on any kind of cryptocurrency or blockchain, and it is incredibly lightweight supporting very high latency and extremely low bandwidth mediums as well as much faster connections.</p>
	<img src="2022-10-26_13-37-59.jpg"
	alt="A graphic listing the names of the different layers of the TCP/IP and RNS models to compare the two; RNS having Reticulum and LXMF."
	title="A graphic listing the names of the different layers of the TCP/IP and RNS models to compare the two; RNS having Reticulum and LXMF.">
</details>
<details open id="Terminology">
	<summary>Terminology</summary>
	<p>Before I show you how to get set up, we must answer an important question; how exactly does Reticulum work?</p>
	<p>Let's start with establishing some basic terminology.</p>
	<p><b>RNS</b> (or Reticulum Network Stack) is the <i>reference implementation</i> of the Reticulum protocol, written in Python.</p>
	<p><b>Destinations</b> are essentially 'endpoints' that allow for programs to receive data. They are more of a routing concept than a reference to a specific program or device, and a single physical device can host <i>multiple</i> different destinations. There are a few types of destinations:</p>
	<ul>
		<li><i>Single Destinations</i> allow for basic packet transmission using asymmetric public key encryption for each packet.
		<li><i>Link Destinations</i> are abstract 'channels' to <i>single destinations</i>, formed by establishing faster symmetric encryption and a set path for more efficient data transfer.
		<li><i>Plain Destinations</i> are for broadcast traffic that needs to be public and not encrypted. Plain data is never transmitted over multiple hops in the network because routing requires encryption to verify paths.
		<li><i>Group Destinations</i> are symmetrically encrypted packets to multiple programs. They are not yet transported over multiple hops, though this is planned future functionality.
	</ul>
	<p><b>Announces</b> are special signed packets that contain a destination's address, public key, and some other information needed to establish end-to-end connectivity to said destination. They are propagated throughout a network by Transport Nodes and allow Reticulum to form multi-hop connections to destinations.</p>
	<p>A <b>Node</b> is an instance of Reticulum. There is generally one per device, and a 'Shared Instance' is created to allow multiple programs to connect at once.</p>
	<p>A <b>Transport Node</b> is an otherwise standard node that is set to route data via a single configuration option. Not all nodes route traffic because this wastes bandwidth and descreases the reliability of larger networks just to support Transport Nodes that will never route anything. Reticulum can technically work without Transport Nodes, but only directly connected nodes are able to communicate without them.</p>
	<p>Nodes do not know anything about a network beyond their immediate neighbors. They can move around a network at will and can even move to completely separate networks and still become reachable. To make a destination reachable, a program simply needs to send an announce on any network it is connected to.</p>
	<p>An <b>Interface</b> is a connection over any medium between two or more nodes.</p>
	<p>And finally <b>Identities</b> represent any kind of verifiable identity of a person, machine control interface, or sensor. Identities are used to <i>create multiple destinations</i> cryptographically linked to that identity.</p>
</details>
<details open id="How_Announces_Work">
	<summary>How Announces Work</summary>
	<p>Transport Nodes forward data to a destination through a route determined by announces, which essentially tell every Transport Node the most efficient <i>next hop</i> for data sent to a specific destination. Destinations do not store any kind of map of the network as that would be incredibly inefficient.</p>
	<p>When an announce for a destination is sent by a program, it will be automatically recorded and forwarded by any transport node, subject to some specific rules defined in the Reticulum manual that I will simplify:</p>
	<ul>
		<li>If the exact announce has not been previously received, the interface the announce was received on and how many hops it has already taken is recorded. This hop count is stored in the announce itself.
		<li>The announce is then queued to be re-transmitted on all interfaces if a maximum hop count requirement is met (by default this is 128 hops).
		<li>Announce bandwith is capped at 2% of an interface's total by default, and announces for destinations that are closest in terms of hops are prioritized, ensuring the reachability and connectivity of local nodes.
	</ul>
	<p>The keys and path to a destination can be requested from any connected Transport Node if a node does not already have an announce from the destination it is trying to reach.</p>
	<p>There are more specifics about the routing, connection, and encryption processes in the Reticulum manual, but these basics should help you form a reasonable understanding of the concepts behind Reticulum's incredibly efficient routing strategy that allows Transport Nodes to directly pass data to a destination <i>without</i> needing to know the network's topology.</p>
</details>
<details open id="Interfaces">
	<summary>Interfaces</summary>
	<p>That being said, you might still want to know what connections Reticulum can work over.</p>
	<p>The answer is basically anything, though there are some limits.</p>
	<p>The <i>reference implementation</i> of Reticulum currently supports connections over any Ethernet, WiFi, KISS mode packet radio, or serial connection, as well I2P, TCP, and UDP. LoRa is also supported via RNode, a digital radio transceiver program designed for things like Reticulum, supporting many common LoRa boards.</p>
	<p>Of course that doesn't mean Reticulum <i>only</i> supports these interfaces, currently any connection faster than 5 <i>bits</i> per second that can send packets 500 bytes or larger are supported, so that means basically anything you can imagine, as long as the work is put in to support it. There is even a very easily utilized generic "pipe" interface, that can use any external program as an interface so you can add new devices <i>without</i> even touching Reticulum code!</p>
	<p>As long as two or more devices are somehow connected together, either directly or through devices with a Transport Node running, they can form a network. Remember that because of Announces, Reticulum is self-organizing and can form a network over any and all of these interfaces simultaneously. Once the interfaces are set up and the necessary nodes are set to be transports, you're basically done with any network configuration!</p>
	<img class="center" src="ReticulumGraphic.png"
	alt="A graphic displaying an example Reticulum network running over multiple different connection types simultaneously."
	title="A graphic displaying an example Reticulum network running over multiple different connection types simultaneously.">
	<p>Interfaces also have special announce propagation rules that can be enabled, known as <a href="https://reticulum.network/manual/interfaces.html#interface-modes">Interface Modes</a>, allowing for more advanced network configuration and helping mitigate edge cases.</p>
</details>
<details open id="LXMF">
	<summary>LXMF</summary>
	<p>So that's great, but what's the point of a network stack that nothing makes use of yet?</p>
	<p>This is why the Reticulum creators made LXMF, a Reticulum-native message format that enables standard long and short form messaging functionality, communication between networked devices, paper messages, and offline reception of messages.</p>
	<p>Offline messaging is facilitated by <b>LXMF Propagation Nodes</b>, which store and forward messages to nodes that are not directly reachable when a message is sent. LXMF Propagation Nodes can also provide infrastructure for distributed bulletin, news, or discussion boards.</p>
	<p>Propagation Nodes will, by default, peer with each other and synchronize messages over time, automatically creating an encrypted, distributed message store. Users can retrieve their messages from any available Propagation Node.</p>
	<p>Propagation Nodes are <i>not</i> required for LXMF to work, they simply serve to add optional functionality that many users will find very useful.</p>
	<p>It is <b>very important</b> to remember that LXMF's Propagation Nodes are <i>not</i> the same as the <i>Transport Nodes</i> that provide Reticulum's core routing functionality.</p>
</details>
<details open id="Comparison_to_Meshtastic">
	<summary>Comparison to Meshtastic</summary>
	<p>Another project you may be familiar with is Meshtastic. It is often compared to Reticulum, even though they are quite different internally.</p>
	<p>Unlike Reticulum, Meshtastic works almost exclusively over LoRa and relies on flood routing. Flood routing essentially sends <i>every single packet</i> to <b>every single node</b>, making the network exponentially less and less efficient as more nodes are added and more messages are sent. In normal use, this means that it takes relatively few nodes sending data to completely overwhelm an entire network. To mitigate this, Meshtastic has a maximum hop limit of around 7 hops (with 3 hops being "strongly recommended") while Reticulum supports <i>128 hops</i>.</p>
	<img class="center" src="Meshtastic-vs-Reticulum.gif"
	alt="An animated GIF comparing the routing strategies of Meshtastic and Reticulum; Meshtastic consuming exponential bandwidth for every extra node on the network and Reticulum having bandwidth scaling nearly proprotionally with the number of nodes on the network."
	title="An animated GIF comparing the routing strategies of Meshtastic and Reticulum; Meshtastic consuming exponential bandwidth for every extra node on the network and Reticulum having bandwidth scaling nearly proprotionally with the number of nodes on the network.">
	<p>Meshtastic's stack can run entirely on a microcontroller, while standard RNodes require a separate device like a Raspberry Pi Zero to handle routing. There is, however, a port of Reticulum to C++ that <i>can</i> run entirely on a microcontroller, called microReticulum, which is already in a relatively usable state.</p>
	<p>Meshtastic's encryption is optional (and only recently gained per-connection encryption) while Reticulum <i>requires</i> per-connection encryption and has forward secrecy, meaning that if a connection's key is compromised, no previous or future connections, even to the same destination, will be compromised. This means that, at least theoretically, the only feasible way to completely break Reticulum's encryption is to compromise the sender or recipient <i>directly</i>.</p>
	<p>Meshtastic is a great project, especially for hyper-mobile LoRa-only communication, and I respect what they're doing, even though I personally find the concepts behind Reticulum much more interesting.</p>
</details>
<details open id="Getting_Started">
	<summary>Getting Started</summary>
	<p>Now that I've explained how Reticulum works, let's try it out!</p>
	<p>There are three major LXMF clients currently available, NomadNet, Sideband, and Reticulum MeshChat.</p>
	<p>NomadNet is a desktop Terminal-based option with some features such as website-esque pages; while Sideband is a GUI app for both desktop and Android which is somewhat easier to use and can send images and files in chats.</p>
	<p>Reticulum MeshChat's goal is to be even easier to use than Sideband, while keeping most of both Sideband and NomadNet's features. It has a very nice interface and adds even more features like voice calling and voice messages, while still working over LoRa.</p>
	<p>To install any of the clients, follow the instructions on their Github pages:<br>
	<i>Reticulum MeshChat</i> - <a href="https://github.com/liamcottle/reticulum-meshchat">https://github.com/liamcottle/reticulum-meshchat</a><br>
	<i>Sideband</i> - <a href="https://github.com/markqvist/Sideband">https://github.com/markqvist/Sideband</a><br>
	<i>NomadNet</i> - <a href="https://github.com/markqvist/nomadnet">https://github.com/markqvist/nomadnet</a></p>
</details>
<details open id="Connectivity_Options">
	<summary>Connectivity Options</summary>
	<p>Once you've picked a client and installed it, you will need to configure some interfaces so you can connect to a larger Reticulum network. In Sideband this is done through the "Connectivity" and "Hardware" pages, with NomadNet it is done through config files in the hidden .nomadnetwork and .reticulum folders in your home directory, and with Reticulum MeshChat it is done through the "Interfaces" page.</p>
	<p>The easiest way to get connected to other people and try out Reticulum is to join the testnet, you can find more info about that at <a href="https://reticulum.network/connect.html">https://reticulum.network/connect.html</a></p>
	<p>Remember that when changing <i>any</i> of these options, you <i>must</i> completely restart your client for changes to apply.</p>
</details>
<details open id="Using_LXMF_Clients">
	<summary>Using LXMF Clients</summary>
	<p>Both NomadNet and Sideband have integrated guides explaining how to access and use their specific features, and I <i>highly</i> recommend reading them to better understand how to use each one.</p>
	<p>You may even discover some less obvious features like Sideband's situation tracking functionality that, if enabled, allows groups to be easily organized and located at a glance, and the Repository page, which allows for the Sideband app to be shared offline!</p>
	<p>For NomadNet specifically, make sure to pay attention to the keyboard shortcuts bar at the bottom of the screen. Each panel has different shortcuts that allow for a ton of functionality that you might otherwise miss.</p>
</details>
<details open id="Conclusion">
	<summary>Conclusion</summary>
	<p>So that's pretty much it for Reticulum's basics. To learn more, I highly recommend taking a look through the Reticulum Manual and Michael Faragher's Reticulum Primer, checking out the resources on Unsigned.io, and chatting with the Reticulum community in the Discussions tab on GitHub and in the Matrix room.</p>
</details>
<details open id="Links">
	<summary>Links</summary>
    <b>Read The FAQ:</b> <a href="https://github.com/markqvist/Reticulum/wiki/Frequently-Asked-Questions">https://github.com/markqvist/Reticulum/wiki/Frequently-Asked-Questions</a><br>
	<b>Mark Qvist's Talk:</b> <a href="https://media.ccc.de/v/38c3-reticulum-unstoppable-networks-for-the-people">https://media.ccc.de/v/38c3-reticulum-unstoppable-networks-for-the-people</a><br><br>
	<b>Reticulum/LXMF Clients:</b>
	<ul>
		<li>Reticulum MeshChat: <a href="https://github.com/liamcottle/reticulum-meshchat">https://github.com/liamcottle/reticulum-meshchat</a>
		<li>Sideband: <a href="https://github.com/markqvist/Sideband">https://github.com/markqvist/Sideband</a>
		<li>NomadNet: <a href="https://github.com/markqvist/NomadNet">https://github.com/markqvist/NomadNet</a>
	</ul>
	<b>Important Resources:</b>
	<ul>
		<li>Reticulum Website: <a href="https://reticulum.network">https://reticulum.network</a>
		<li>Reticulum Manual: <a href="https://reticulum.network/manual/index.html">https://reticulum.network/manual/index.html</a>
		<li>Unsigned.io Guides: <a href="https://unsigned.io">https://unsigned.io</a> (Mirror: <a href="https://reticulum.betweentheborders.com">https://reticulum.betweentheborders.com</a>)
		<li>Michael Faragher's Reticulum Primer: <a href="https://reticulum.betweentheborders.com/primer.pdf">https://reticulum.betweentheborders.com/primer.pdf</a>
	</ul>
	<b>Reticulum Community:</b>
	<ul>
		<li>GitHub Discussions Page: <a href="https://github.com/markqvist/Reticulum/discussions">https://github.com/markqvist/Reticulum/discussions</a>
		<li>Reticulum Matrix Room: <a href="https://matrix.to/#/#reticulum:matrix.org">https://matrix.to/#/#reticulum:matrix.org</a>
	</ul>
	<b>Some More Resources:</b>
	<ul>
		<li>Michael Faragher's Sideband Situation Tracker Guide: <a href="https://reticulum.betweentheborders.com/guidance.pdf">https://reticulum.betweentheborders.com/guidance.pdf</a>
		<li>Useful Reticulum Presentation: <a href="https://r8io.github.io/rns-presentations">https://r8io.github.io/rns-presentations</a>
		<li>Awesome Reticulum: <a href="https://github.com/markqvist/Reticulum/wiki/Awesome-Reticulum">https://github.com/markqvist/Reticulum/wiki/Awesome-Reticulum</a>
		<li>Let's Decentralize: <a href="https://letsdecentralize.org/">https://letsdecentralize.org/</a>
		<li>OSI Comparison Source: <a href="https://matrix.to/#/#reticulum:matrix.org/$HlXJHhXvbCx6ILYCpKa04zaDVPw89eNhoI7pMR_UZyk">https://matrix.to/#/#reticulum:matrix.org/$HlXJHhXvbCx6ILYCpKa04zaDVPw89eNhoI7pMR_UZyk</a>
	</ul>
	<b>Other Git Repos:</b>
	<ul>
		<li>Reticulum: <a href="https://github.com/markqvist/Reticulum">https://github.com/markqvist/Reticulum</a>
		<li>LXMF: <a href="https://github.com/markqvist/LXMF">https://github.com/markqvist/LXMF</a>
		<li>RNode: <a href="https://github.com/markqvist/RNode_Firmware">https://github.com/markqvist/RNode_Firmware</a>
		<li>RNode CE: <a href="https://github.com/liberatedsystems/RNode_Firmware_CE">https://github.com/liberatedsystems/RNode_Firmware_CE</a>
		<li>C++ Reticulum Port: <a href="https://github.com/attermann/microReticulum">https://github.com/attermann/microReticulum</a>
		<li>C++ Port Firmware: <a href="https://github.com/attermann/microReticulum_Firmware">https://github.com/attermann/microReticulum_Firmware</a>
		<li>RNode Web Flasher: <a href="https://liamcottle.github.io/rnode-flasher">https://liamcottle.github.io/rnode-flasher</a>
	</ul>
</details>
<footer>
<a href="/">Home</a> - <a href="#top">Top</a>
</footer>
</body>
